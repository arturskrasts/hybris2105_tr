package translators;

import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import org.apache.commons.lang.StringUtils;
import org.training.core.jalo.EcentaNotification;

public class NotificationTitleValueTranslator extends AbstractValueTranslator {
    @Override
    public Object importValue(final String valueExpr, final Item toItem) throws JaloInvalidParameterException {
        clearStatus();
        String result = null;
        if (StringUtils.isBlank(valueExpr)) {
            final EcentaNotification ecentaNotification = (EcentaNotification) toItem;
            String message = ecentaNotification.getMessage();
            if (!StringUtils.isBlank(message)) {
                if (message.length() > 100) {
                    result = message.substring(0, 100);
                } else {
                    result = message;
                }
            }
        } else {
            result = valueExpr;
        }
        return result;
    }

    @Override
    public String exportValue(final Object value) throws JaloInvalidParameterException {
        return value == null ? "" : value.toString();
    }
}
