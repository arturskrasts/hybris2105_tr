package decorators;

import de.hybris.platform.util.CSVCellDecorator;

import java.util.Map;

public class MessageCellDecorator implements CSVCellDecorator
{
    @Override
    public String decorate(final int position, final Map<Integer, String> srcLine)
    {
        final String csvCell = srcLine.get(Integer.valueOf(position));
        return csvCell + " Sample data";
    }
}