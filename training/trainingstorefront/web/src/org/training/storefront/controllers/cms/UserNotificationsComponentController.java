package org.training.storefront.controllers.cms;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.services.impl.DefaultB2BCustomerService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.training.core.dao.CustomEcentaNotificationsDAO;
import org.training.core.model.EcentaNotificationModel;
import org.training.core.model.UserNotificationsComponentModel;
import org.training.storefront.controllers.ControllerConstants;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@Controller("UserNotificationsComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.UserNotificationsComponent)
public class UserNotificationsComponentController extends AbstractAcceleratorCMSComponentController<UserNotificationsComponentModel>
{
    private static final String NOTIFICATION_LIMIT = "notificationLimit";

    @Resource(name = "customEcentaNotificationsDAO")
    private CustomEcentaNotificationsDAO customEcentaNotificationsDAO;
    @Resource(name = "defaultB2BCustomerService")
    private DefaultB2BCustomerService defaultB2BCustomerService;
    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Override
    protected void fillModel(HttpServletRequest request, Model model, UserNotificationsComponentModel component) {
        String remoteUser = request.getRemoteUser();
        if(remoteUser != null){
            int notificationLimit = configurationService.getConfiguration().getInt(NOTIFICATION_LIMIT);
            B2BCustomerModel b2bCustomer = defaultB2BCustomerService.getUserForUID(remoteUser);
            List<EcentaNotificationModel> ecentaNotificationModelList = new ArrayList<>(customEcentaNotificationsDAO.findAllEcentaNotificationsFromSpecificB2BCustomer(b2bCustomer));
            ecentaNotificationModelList.removeIf(EcentaNotificationModel::getDeleted);
            sortNotificationsByDate(ecentaNotificationModelList);
            List<EcentaNotificationModel> limitedEcentaNotificationModelList = ecentaNotificationModelList.stream().limit(notificationLimit).collect(Collectors.toList());
            model.addAttribute("ecentaNotificationList", limitedEcentaNotificationModelList);
        }
    }

    private void sortNotificationsByDate(List<EcentaNotificationModel> ecentaNotificationModelList) {
        Collections.sort(ecentaNotificationModelList, new NotificationsDateComparator());
    }

    class NotificationsDateComparator implements Comparator<EcentaNotificationModel> {

        @Override
        public int compare(EcentaNotificationModel o1, EcentaNotificationModel o2) {
            if(o1 != null && o2 != null && o1.getDate() != null && o2.getDate() != null) {
                return o2.getDate().compareTo(o1.getDate());
            }
            return 0;
        }
    }

    public void setCustomEcentaNotificationsDAO(CustomEcentaNotificationsDAO customEcentaNotificationsDAO) {
        this.customEcentaNotificationsDAO = customEcentaNotificationsDAO;
    }

    public void setDefaultB2BCustomerService(DefaultB2BCustomerService defaultB2BCustomerService) {
        this.defaultB2BCustomerService = defaultB2BCustomerService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}
