package org.training.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Scope("tenant")
@RequestMapping("/notifications")
public class NotificationsOverviewPageController extends AbstractPageController {
    private static final String NOTIFICATIONS_CMS_PAGE = "notificationsOverviewCMSPage";

    @RequestMapping(method = RequestMethod.GET)
    @RequireHardLogIn
    public String getNotificationsCMSPage(final Model model) throws CMSItemNotFoundException
    {
        final ContentPageModel notificationsOverviewCMSPage = getContentPageForLabelOrId(NOTIFICATIONS_CMS_PAGE);
        storeCmsPageInModel(model, notificationsOverviewCMSPage);
        setUpMetaDataForContentPage(model, notificationsOverviewCMSPage);
        return getViewForPage(model);
    }
}
