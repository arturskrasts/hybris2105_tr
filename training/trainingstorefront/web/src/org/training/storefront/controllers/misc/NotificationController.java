package org.training.storefront.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.training.core.crud.EcentaNotificationCRUD;
import org.training.core.dao.CustomEcentaNotificationsDAO;
import org.training.core.model.EcentaNotificationModel;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class NotificationController extends AbstractController {

    @Resource(name = "ecentaNotificationCRUD")
    private EcentaNotificationCRUD ecentaNotificationCRUD;
    @Resource(name = "customEcentaNotificationsDAO")
    private CustomEcentaNotificationsDAO customEcentaNotificationsDAO;

    @RequestMapping(value = "/notifications/read", method = RequestMethod.POST)
    public void readNotification(final HttpServletRequest request, final HttpServletResponse response){
        String id = request.getParameter("notificationId");
        EcentaNotificationModel ecentaNotificationModel = customEcentaNotificationsDAO.findEcentaNotificationWithId(id);
        ecentaNotificationModel.setRead(true);
        ecentaNotificationCRUD.updateEcentaNotification(ecentaNotificationModel);
    }

    @RequestMapping(value = "/notifications/delete", method = RequestMethod.POST)
    public void deleteNotification(final HttpServletRequest request, final HttpServletResponse response){
        String id = request.getParameter("notificationId");
        EcentaNotificationModel ecentaNotificationModel = customEcentaNotificationsDAO.findEcentaNotificationWithId(id);
        ecentaNotificationModel.setDeleted(true);
        ecentaNotificationCRUD.updateEcentaNotification(ecentaNotificationModel);
    }

    public void setEcentaNotificationCRUD(EcentaNotificationCRUD ecentaNotificationCRUD) {
        this.ecentaNotificationCRUD = ecentaNotificationCRUD;
    }

    public void setCustomEcentaNotificationsDAO(CustomEcentaNotificationsDAO customEcentaNotificationsDAO) {
        this.customEcentaNotificationsDAO = customEcentaNotificationsDAO;
    }
}
