<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/responsive/nav/breadcrumb" %>

<c:if test="${user.getUid() != 'anonymous'}">
    <div class="row" style="margin:10px 0">
        <button class="col-md-2 js-show-all-notifications filter-notifications" id="SHOWALL">
            SHOW ALL<span id="all-unread" hidden>0</span>
        </button>
        <button class="col-md-2 js-hide-notifications filter-notifications" id="HIGH" style="text-decoration: underline;">
            HIGH PRIORITY<span id="high-unread" hidden>0</span>
        </button>
        <button class="col-md-3 js-hide-notifications filter-notifications" id="ORDERMANAGEMENT">
            ORDER MANAGEMENT<span id="order-unread" hidden>0</span>
        </button>
        <button class="col-md-1 js-hide-notifications filter-notifications" id="NEWS">
            NEWS<span id="news-unread" hidden>0</span>
        </button>
        <button class="col-md-2 js-hide-notifications filter-notifications" id="SERVICETICKETS">
            SERVICE TICKETS<span id="service-unread" hidden>0</span>
        </button>
        <button class="col-md-2 js-hide-notifications filter-notifications" id="WORKFLOW">
            WORKFLOW<span id="workflow-unread" hidden>0</span>
        </button>
    </div>

    <c:forEach var="ecentaNotification" items="${ecentaNotificationList}" varStatus="loop">
        <c:set value="${ecentaNotification.getPriority()} ${ecentaNotification.getType()} read.${ecentaNotification.getRead()} deleted.${ecentaNotification.getDeleted() }" var="classTags"/>
        <div class="row notifications ${classTags}" id="${ecentaNotification.getId()}" style="margin-top:10px">
            <div class="col-md-1" style="text-align: center;">
                <c:choose>
                    <c:when test="${ecentaNotification.getPriority() == 'HIGH'}">
                        <img src="${fn:escapeXml(commonResourcePath)}/images/high_priority_round.png" width="25" height="25" />
                    </c:when>
                    <c:when test="${ecentaNotification.getPriority() == 'NORMAL'}">
                        <img src="${fn:escapeXml(commonResourcePath)}/images/normal_priority_round.png" width="25" height="25" />
                    </c:when>
                    <c:otherwise>
                        <img src="${fn:escapeXml(commonResourcePath)}/images/low_priority_round.png" width="25" height="25" />
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="col-md-2">
                <c:choose>
                    <c:when test="${ecentaNotification.getRead() == 'false'}">
                        <p class="creation-date" style="font-weight: bold;"><fmt:formatDate pattern = "dd-MM-yyyy" value = "${ecentaNotification.getDate()}" /></p>
                    </c:when>
                    <c:otherwise>
                        <p class="creation-date"><fmt:formatDate pattern = "dd-MM-yyyy" value = "${ecentaNotification.getDate()}" /></p>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="col-md-7">
                 <c:choose>
                     <c:when test="${ecentaNotification.getRead() == 'false'}">
                         <p class="notification-message" style="font-weight: bold;">"${ecentaNotification.getMessage()}"</p>
                     </c:when>
                     <c:otherwise>
                         <p class="notification-message">"${ecentaNotification.getMessage()}"</p>
                     </c:otherwise>
                 </c:choose>
            </div>
            <div class="col-md-1" style="text-align: center;">
                <button class="js-delete-notification" disabled>
                    <img src="${fn:escapeXml(commonResourcePath)}/images/delete_round.png" width="25" height="25" style="opacity: 0.5;"/>
                </button>
            </div>
            <div class="col-md-1" style="text-align: center;">
                <button class="js-mark-as-read">
                    <img src="${fn:escapeXml(commonResourcePath)}/images/read_round.png" width="25" height="25" />
                </button>
            </div>
        </div>
    </c:forEach>
</c:if>

