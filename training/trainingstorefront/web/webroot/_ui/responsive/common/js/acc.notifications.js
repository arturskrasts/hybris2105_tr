

ACC.notifications = {

    _autoload: [
        "showAll",
        "hideWithoutClass",
        "deleteNotification",
        "markAsRead",
        "ifNotificationIsRead",
        "countUnreadNotifications",
        "showOnlyHighPriority"
    ],

    showAll: function(){
        $('.js-show-all-notifications').click(function(){
            $(".filter-notifications").css('text-decoration', 'none');
            $(this).css('text-decoration', 'underline');
            var notifications = $(".notifications");
            notifications.show();
        });
    },

    hideWithoutClass: function(){
        $('.js-hide-notifications').click(function(){
            $(".filter-notifications").css('text-decoration', 'none');
            $(this).css('text-decoration', 'underline');
            var notifications = $(".notifications");
            notifications.hide();
            var filter = "." + $(this).attr('id');
            notifications.filter(filter).show();
        });
    },

    deleteNotification: function(){
        $('.js-delete-notification').click(function(){
            $(this).parents(".notifications").remove();
            var id = $(this).parents(".notifications").attr('id');
            $.ajax({
                url: "./notifications/delete",
                type: 'POST',
                data: {"notificationId": id},
                success: function (data) {
                },
                error: function (data) {
                }
            });
        });
    },

    markAsRead: function(){
        $('.js-mark-as-read').click(function(){
          $(this).parents(".notifications").find(".creation-date").css('font-weight', 'normal');
          $(this).parents(".notifications").find(".notification-message").css('font-weight', 'normal');
          ACC.notifications.updateUnreadNumbers($(this).parents(".notifications"));
          $(this).parents(".notifications").find(".js-delete-notification").prop("disabled",false);
          $(this).parents(".notifications").find(".js-delete-notification").children("img").css('opacity', 1);
          $(this).prop("disabled",true);
          $(this).children("img").css('opacity', 0.5);
          var id = $(this).parents(".notifications").attr('id');
          $.ajax({
              url: "./notifications/read",
              type: 'POST',
              data: {"notificationId": id},
              success: function (data) {
              },
              error: function (data) {
              }
          });
        });
    },

    updateUnreadNumbers: function(notification){
        ACC.notifications.updateNumber("#all-unread", -1);
        if(notification.hasClass("HIGH")) {
            ACC.notifications.updateNumber("#high-unread", -1);
        }
        if(notification.hasClass("ORDERMANAGEMENT")) {
            ACC.notifications.updateNumber("#order-unread", -1);
        }
        if(notification.hasClass("NEWS")) {
            ACC.notifications.updateNumber("#news-unread", -1);
        }
        if(notification.hasClass("SERVICETICKETS")) {
            ACC.notifications.updateNumber("#service-unread", -1);
        }
        if(notification.hasClass("WORKFLOW")) {
            ACC.notifications.updateNumber("#workflow-unread", -1);
        }
    },

    ifNotificationIsRead: function(){
        $(".notifications").each(function(index){
            if($(this).hasClass("read.true")){
                $(this).find(".js-delete-notification").prop("disabled",false);
                $(this).find(".js-delete-notification").children("img").css('opacity', 1);
                $(this).find(".js-mark-as-read").prop("disabled",true);
                $(this).find(".js-mark-as-read").children("img").css('opacity', 0.5);
            }
        });
    },

    countUnreadNotifications: function(){
        $(".notifications").each(function(index){
            if($(this).hasClass("read.false")){
               ACC.notifications.updateNumber("#all-unread", 1);
               if($(this).hasClass("HIGH")) {
                  ACC.notifications.updateNumber("#high-unread", 1);
               }
               if($(this).hasClass("ORDERMANAGEMENT")) {
                   ACC.notifications.updateNumber("#order-unread", 1);
               }
               if($(this).hasClass("NEWS")) {
                  ACC.notifications.updateNumber("#news-unread", 1);
               }
               if($(this).hasClass("SERVICETICKETS")) {
                   ACC.notifications.updateNumber("#service-unread", 1);
               }
               if($(this).hasClass("WORKFLOW")) {
                   ACC.notifications.updateNumber("#workflow-unread", 1);
               }
            }
        });
    },

     updateNumber: function(id, i){
        var unread = parseInt($(id).text());
        if(unread == 0 && i == 1){
            $(id).show();
        }
        unread += i;
        if(unread == 0){
            $(id).hide();
        }
        else{
            $(id).text(" " + unread);
        }
    },

    showOnlyHighPriority: function(){
        $("#HIGH").css('text-decoration', 'underline');
        var notifications = $(".notifications");
        notifications.hide();
        var filter = ".HIGH";
        notifications.filter(filter).show();
    }
};