package org.training.core.dao.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.training.core.dao.CustomEcentaNotificationsDAO;
import org.training.core.enums.EcentaNotificationType;
import org.training.core.enums.NotificationPriority;
import org.training.core.model.EcentaNotificationModel;

import java.util.*;

public class CustomEcentaNotificationsDAOImpl implements CustomEcentaNotificationsDAO {

    private FlexibleSearchService flexibleSearchService;

    private static final String QUERY_PARAM_OLDDATE = "oldDate";
    private static final String QUERY_PARAM_B2BCUSTOMER = "b2bCustomerModel";
    private static final String QUERY_PARAM_PRIORITY = "priority";
    private static final String QUERY_PARAM_TYPE = "type";
    private static final String QUERY_PARAM_ID = "id";

    private static final String QUERY_START = "SELECT {" + EcentaNotificationModel.PK + "} FROM {" + EcentaNotificationModel._TYPECODE + "}";
    private static final String QUERY_WHERE_DATE = "WHERE {" + EcentaNotificationModel.DATE + "}<=?oldDate";
    private static final String QUERY_WHERE_B2BCUSOMER = "WHERE {" + EcentaNotificationModel.B2BCUSTOMER + "}=?b2bCustomerModel";
    private static final String QUERY_WHERE_ID = "WHERE {" + EcentaNotificationModel.ID + "}=?id";
    private static final String QUERY_AND_PRIORITY = "AND {" + EcentaNotificationModel.PRIORITY + "}='?priority";
    private static final String QUERY_AND_TYPE = "AND {" + EcentaNotificationModel.TYPE + "}='?type";

    @Override
    public List<EcentaNotificationModel> findAllEcentaNotificationsOlderThanXDays(final Date oldDate) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(QUERY_PARAM_OLDDATE, oldDate);
        return getEcentaNotificationList(QUERY_START + QUERY_WHERE_DATE, params);
    }

    @Override
    public List<EcentaNotificationModel> findAllEcentaNotificationsFromSpecificB2BCustomer(B2BCustomerModel b2bCustomerModel) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(QUERY_PARAM_B2BCUSTOMER, b2bCustomerModel);
        return getEcentaNotificationList(QUERY_START + QUERY_WHERE_B2BCUSOMER, params);
    }

    @Override
    public List<EcentaNotificationModel> findAllHighPriorityEcentaNotificationsFromSpecificB2BCustomer(B2BCustomerModel b2bCustomerModel) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(QUERY_PARAM_B2BCUSTOMER, b2bCustomerModel);
        params.put(QUERY_PARAM_PRIORITY, NotificationPriority.HIGH);
        return getEcentaNotificationList(QUERY_START + QUERY_WHERE_B2BCUSOMER + QUERY_AND_PRIORITY, params);
    }

    @Override
    public List<EcentaNotificationModel> findAllNewsEcentaNotificationsFromSpecificB2BCustomer(B2BCustomerModel b2bCustomerModel) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(QUERY_PARAM_B2BCUSTOMER, b2bCustomerModel);
        params.put(QUERY_PARAM_TYPE, EcentaNotificationType.NEWS);
        return getEcentaNotificationList(QUERY_START + QUERY_WHERE_B2BCUSOMER + QUERY_AND_TYPE, params);
    }

    @Override
    public List<EcentaNotificationModel> findAllOrderManagementEcentaNotificationsFromSpecificB2BCustomer(B2BCustomerModel b2bCustomerModel) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(QUERY_PARAM_B2BCUSTOMER, b2bCustomerModel);
        params.put(QUERY_PARAM_TYPE, EcentaNotificationType.ORDERMANAGEMENT);
        return getEcentaNotificationList(QUERY_START + QUERY_WHERE_B2BCUSOMER + QUERY_AND_TYPE, params);
    }

    @Override
    public List<EcentaNotificationModel> findAllServiceTicketsEcentaNotificationsFromSpecificB2BCustomer(B2BCustomerModel b2bCustomerModel) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(QUERY_PARAM_B2BCUSTOMER, b2bCustomerModel);
        params.put(QUERY_PARAM_TYPE, EcentaNotificationType.SERVICETICKETS);
        return getEcentaNotificationList(QUERY_START + QUERY_WHERE_B2BCUSOMER + QUERY_AND_TYPE, params);
    }

    @Override
    public List<EcentaNotificationModel> findAllWorkflowEcentaNotificationsFromSpecificB2BCustomer(B2BCustomerModel b2bCustomerModel) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(QUERY_PARAM_B2BCUSTOMER, b2bCustomerModel);
        params.put(QUERY_PARAM_TYPE, EcentaNotificationType.WORKFLOW);
        return getEcentaNotificationList(QUERY_START + QUERY_WHERE_B2BCUSOMER + QUERY_AND_TYPE, params);
    }

    @Override
    public EcentaNotificationModel findEcentaNotificationWithId(String id) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(QUERY_PARAM_ID, id);
        List<EcentaNotificationModel> ecentaNotificationModelList = getEcentaNotificationList(QUERY_START + QUERY_WHERE_ID, params);
        return ecentaNotificationModelList.get(0);
    }

    protected List<EcentaNotificationModel> getEcentaNotificationList(String query, Map<String, Object> params) {
        final SearchResult<EcentaNotificationModel> searchResult = flexibleSearchService.search(query, params);
        return searchResult.getResult();
    }

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}
