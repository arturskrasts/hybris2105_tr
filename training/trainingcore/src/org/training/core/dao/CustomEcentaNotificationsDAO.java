package org.training.core.dao;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import org.training.core.model.EcentaNotificationModel;

import java.util.Date;
import java.util.List;

public interface CustomEcentaNotificationsDAO {
    List<EcentaNotificationModel> findAllEcentaNotificationsOlderThanXDays(final Date oldDate);

    List<EcentaNotificationModel> findAllEcentaNotificationsFromSpecificB2BCustomer(final B2BCustomerModel b2bCustomerModel);

    List<EcentaNotificationModel> findAllHighPriorityEcentaNotificationsFromSpecificB2BCustomer(final B2BCustomerModel b2bCustomerModel);

    List<EcentaNotificationModel> findAllOrderManagementEcentaNotificationsFromSpecificB2BCustomer(final B2BCustomerModel b2bCustomerModel);

    List<EcentaNotificationModel> findAllNewsEcentaNotificationsFromSpecificB2BCustomer(final B2BCustomerModel b2bCustomerModel);

    List<EcentaNotificationModel> findAllServiceTicketsEcentaNotificationsFromSpecificB2BCustomer(final B2BCustomerModel b2bCustomerModel);

    List<EcentaNotificationModel> findAllWorkflowEcentaNotificationsFromSpecificB2BCustomer(final B2BCustomerModel b2bCustomerModel);

    EcentaNotificationModel findEcentaNotificationWithId(String id);
}
