package org.training.core.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.log4j.Logger;
import org.training.core.crud.EcentaNotificationCRUD;
import org.training.core.dao.CustomEcentaNotificationsDAO;
import org.training.core.model.EcentaNotificationModel;
import org.training.core.model.EcentaNotificationRemovalCronJobModel;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EcentaNotificationsRemovalJob extends AbstractJobPerformable<EcentaNotificationRemovalCronJobModel> {
    private CustomEcentaNotificationsDAO customEcentaNotificationsDAO;
    private ModelService modelService;
    private EcentaNotificationCRUD ecentaNotificationCRUD;

    private final static Logger LOG = Logger.getLogger(EcentaNotificationsRemovalJob.class.getName());

    @Override
    public PerformResult perform(final EcentaNotificationRemovalCronJobModel ecentaNotificationRemovalCronJobModel) {
        final int xDaysOldToRemove = ecentaNotificationRemovalCronJobModel.getXDaysOld();
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -xDaysOldToRemove);
        final Date oldDateThreshold = calendar.getTime();
        final List<EcentaNotificationModel> ecentaNotificationModelListToEdit = customEcentaNotificationsDAO.findAllEcentaNotificationsOlderThanXDays(oldDateThreshold);
        LOG.debug("Ecenta notifications older than specified days size:" + ecentaNotificationModelListToEdit.size());

        for (final EcentaNotificationModel ecentaNotificationModel : ecentaNotificationModelListToEdit) {
            ecentaNotificationModel.setRead(true);
            ecentaNotificationModel.setDeleted(true);
            ecentaNotificationCRUD.updateEcentaNotification(ecentaNotificationModel);
        }
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    public CustomEcentaNotificationsDAO getCustomEcentaNotificationsDAO() {
        return customEcentaNotificationsDAO;
    }

    public void setCustomEcentaNotificationsDAO(final CustomEcentaNotificationsDAO customEcentaNotificationsDAO) {
        this.customEcentaNotificationsDAO = customEcentaNotificationsDAO;
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Override
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public EcentaNotificationCRUD getEcentaNotificationCRUD() {
        return ecentaNotificationCRUD;
    }

    public void setEcentaNotificationCRUD(final  EcentaNotificationCRUD ecentaNotificationCRUD) {
        this.ecentaNotificationCRUD = ecentaNotificationCRUD;
    }
}
