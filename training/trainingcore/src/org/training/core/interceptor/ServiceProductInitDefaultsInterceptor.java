package org.training.core.interceptor;

import de.hybris.platform.servicelayer.interceptor.InitDefaultsInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import org.training.core.model.ServiceProductModel;

import java.util.Calendar;
import java.util.Date;

public class ServiceProductInitDefaultsInterceptor implements InitDefaultsInterceptor<ServiceProductModel> {

    private PersistentKeyGenerator serviceProductIdGenerator;

    @Override
    public void onInitDefaults(ServiceProductModel serviceProductModel, InterceptorContext interceptorContext) throws InterceptorException {
        String generatedId = getServiceProductIdGenerator().generate().toString();
        Calendar calendar = Calendar.getInstance();
        Date creationDate = calendar.getTime();
        serviceProductModel.setId(generatedId);
        serviceProductModel.setCreationDate(creationDate);
    }

    public PersistentKeyGenerator getServiceProductIdGenerator() {
        return serviceProductIdGenerator;
    }

    public void setServiceProductIdGenerator(PersistentKeyGenerator serviceProductIdGenerator) {
        this.serviceProductIdGenerator = serviceProductIdGenerator;
    }


}
