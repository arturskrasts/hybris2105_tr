package org.training.core.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.training.core.model.EcentaNotificationModel;

public class EcentaNotificationPrepareInterceptor implements PrepareInterceptor<EcentaNotificationModel> {
    @Override
    public void onPrepare(EcentaNotificationModel ecentaNotificationModel, InterceptorContext interceptorContext) throws InterceptorException {
        boolean isRead = ecentaNotificationModel.getRead();
        if(!isRead) {
            ecentaNotificationModel.setDeleted(false);
        }
    }
}
