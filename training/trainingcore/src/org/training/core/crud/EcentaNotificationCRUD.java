package org.training.core.crud;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import org.training.core.enums.EcentaNotificationType;
import org.training.core.enums.NotificationPriority;
import org.training.core.model.EcentaNotificationModel;

public interface EcentaNotificationCRUD {
    EcentaNotificationModel createEcentaNotification();

    void updateEcentaNotification(final EcentaNotificationModel ecentaNotificationModel);

    EcentaNotificationModel cloneEcentaNotification(final EcentaNotificationModel ecentaNotificationModel);

    void deleteEcentaNotification(final EcentaNotificationModel ecentaNotificationModel);
}
