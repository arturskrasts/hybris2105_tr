package org.training.core.crud.impl;

import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.Transaction;
import org.training.core.crud.EcentaNotificationCRUD;
import org.training.core.model.EcentaNotificationModel;

public class EcentaNotificationCRUDImpl implements EcentaNotificationCRUD {

    private ModelService modelService;

    private PersistentKeyGenerator ecentaNotificationIdGenerator;

    @Override
    public EcentaNotificationModel createEcentaNotification() {
        EcentaNotificationModel ecentaNotificationModel;
        Transaction tx = Transaction.current();
        boolean success = false;
        tx.begin();
        try {
            ecentaNotificationModel = modelService.create(EcentaNotificationModel.class);
            success = true;
        } finally {
            if (success) {
                tx.commit();
            } else {
                tx.rollback();
            }
        }

        return ecentaNotificationModel;
    }

    @Override
    public void updateEcentaNotification(final EcentaNotificationModel ecentaNotificationModel) {
        Transaction tx = Transaction.current();
        boolean success = false;
        tx.begin();
        try {
            modelService.save(ecentaNotificationModel);
            success = true;
        } finally {
            if (success) {
                tx.commit();
            } else {
                tx.rollback();
            }
        }
    }

    @Override
    public EcentaNotificationModel cloneEcentaNotification(final EcentaNotificationModel ecentaNotificationModel) {
        EcentaNotificationModel clonedEcentaNotificationModel;
        Transaction tx = Transaction.current();
        boolean success = false;
        tx.begin();
        try {
            clonedEcentaNotificationModel = modelService.clone(ecentaNotificationModel);
            clonedEcentaNotificationModel.setId(getEcentaNotificationIdGenerator().generate().toString());
            modelService.save(clonedEcentaNotificationModel);
            success = true;
        } finally {
            if (success) {
                tx.commit();
            } else {
                tx.rollback();
            }
        }
        return clonedEcentaNotificationModel;
    }

    @Override
    public void deleteEcentaNotification(final EcentaNotificationModel ecentaNotificationModel) {
        Transaction tx = Transaction.current();
        boolean success = false;
        tx.begin();
        try {
            modelService.remove(ecentaNotificationModel);
            success = true;
        } finally {
            if (success) {
                tx.commit();
            } else {
                tx.rollback();
            }
        }
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public PersistentKeyGenerator getEcentaNotificationIdGenerator() {
        return ecentaNotificationIdGenerator;
    }

    public void setEcentaNotificationIdGenerator(PersistentKeyGenerator ecentaNotificationIdGenerator) {
        this.ecentaNotificationIdGenerator = ecentaNotificationIdGenerator;
    }
}
