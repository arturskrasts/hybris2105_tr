package org.training.core.attributes;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import org.training.core.model.EcentaNotificationModel;

import java.util.Set;

public class EcentaNotificationB2BUnitHandler extends AbstractDynamicAttributeHandler<B2BUnitModel, EcentaNotificationModel> {

    @Override
    public B2BUnitModel get(final EcentaNotificationModel ecentaNotificationModel) {
        B2BUnitModel b2bUnit = null;
        B2BCustomerModel b2bCustomerModel = ecentaNotificationModel.getB2bCustomer();
        Set<PrincipalGroupModel> b2bCustomerModelGroups = b2bCustomerModel.getGroups();
        for (final PrincipalGroupModel b2bCustomerModelGroup : b2bCustomerModelGroups) {
            if (b2bCustomerModelGroup.getClass() == B2BUnitModel.class) {
                b2bUnit = (B2BUnitModel) b2bCustomerModelGroup;
            }
        }
        return b2bUnit;
    }
}
