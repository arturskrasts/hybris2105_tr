package org.training.facades.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.beans.factory.annotation.Required;
import org.training.core.model.EcentaNotificationModel;
import org.training.facades.product.data.EcentaNotificationData;

public class EcentaNotificationDataPopulator implements Populator<EcentaNotificationModel, EcentaNotificationData> {

    private Converter<B2BCustomerModel, CustomerData> b2BCustomerDataConverter;
    private Converter<B2BUnitModel, B2BUnitData> b2BUnitDataConverter;

    @Override
    public void populate(EcentaNotificationModel ecentaNotificationModel, EcentaNotificationData ecentaNotificationData) throws ConversionException {
        ecentaNotificationData.setId(ecentaNotificationModel.getId());
        ecentaNotificationData.setB2bCustomer(b2BCustomerDataConverter.convert(ecentaNotificationModel.getB2bCustomer()));
        ecentaNotificationData.setDate(ecentaNotificationModel.getDate());
        ecentaNotificationData.setType(ecentaNotificationModel.getType());
        ecentaNotificationData.setMessage(ecentaNotificationModel.getMessage());
        ecentaNotificationData.setPriority(ecentaNotificationModel.getPriority());
        ecentaNotificationData.setRead(ecentaNotificationModel.getRead());
        ecentaNotificationData.setDeleted(ecentaNotificationModel.getDeleted());
        ecentaNotificationData.setTitle(ecentaNotificationModel.getTitle());
        ecentaNotificationData.setB2bUnit(b2BUnitDataConverter.convert(ecentaNotificationModel.getB2bUnit()));
    }

    protected Converter<B2BCustomerModel, CustomerData> getB2BCustomerDataConverter()
    {
        return b2BCustomerDataConverter;
    }

    @Required
    public void setB2BCustomerDataConverter(final Converter<B2BCustomerModel, CustomerData> b2BCustomerDataConverter)
    {
        this.b2BCustomerDataConverter = b2BCustomerDataConverter;
    }

    protected Converter<B2BUnitModel, B2BUnitData> getB2BUnitDataConverter()
    {
        return b2BUnitDataConverter;
    }

    @Required
    public void setB2BUnitDataConverter(final Converter<B2BUnitModel, B2BUnitData> b2BUnitDataConverter)
    {
        this.b2BUnitDataConverter = b2BUnitDataConverter;
    }
}
